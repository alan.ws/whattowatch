import React, { useState } from 'react'

function Search() {
    const [tvShow, setTvShow] = useState(undefined)
    const [hasSearched, setHasSearched] = useState(false)
    const [searchInProgress, setSearchInProgress] = useState(false)

    async function retrieveTvShowData() {

    }

    function somePeopleUseButtons() {
        setSearchInProgress(true)
        setTimeout(() => { setHasSearched(true) }, 1000)
    }

    function somePeopleUseEnter(e) {
        if (e.key === 'Enter') {
            setSearchInProgress(true)
            setTimeout(() => { setHasSearched(true) }, 1000)
        }
    }

    // TODO: cant find the solution currently, but I would like something like the following
    // function Header() {
    //     return (
    //         <header>
    //             My TV Shows
    //             <input
    //                 defaultValue={tvShow}
    //                 onChange={(e) => setTvShow(e.target.value)}
    //                 onKeyPress={(e) => somePeopleUseEnter(e)} />
    //             <button onClick={somePeopleUseButtons}>search</button>
    //         </header>
    //     )
    // }
    // function WithHeader({ component: Component }) {
    //     return (
    //         <>
    //             <Header />
    //             <Component />
    //         </>
    //     )
    // }

    function extractTvShowData() { }
    function renderTvShows() { }

    return (
        <>
            <header>
                My TV Shows
                <input
                    defaultValue={tvShow}
                    onChange={(e) => setTvShow(e.target.value)}
                    onKeyPress={(e) => somePeopleUseEnter(e)} />
                <button onClick={somePeopleUseButtons}>search</button>
            </header>
            {
                hasSearched
                    ? <table>
                        <thead>
                            <tr>
                                <th>Cover</th>
                                <th>Title</th>
                                <th>Year</th>
                                <th>Rate</th>
                                <th>Long</th>
                                <th>Add/Remove Watchlist</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Ringo Starr</td>
                                <td>Drums</td>
                            </tr>
                        </tbody>
                    </table>
                    : searchInProgress
                        ? <h1>Searching</h1>
                        : <h1>Enter something</h1>
            }
        </>
    )
}

export default Search
