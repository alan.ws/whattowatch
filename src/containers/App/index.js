import App from './App.jsx'
import { connect } from 'react-redux'
import { bindActionCreators } from "redux"
import { setAuthenticated } from '../../actions/authentication'

const mapStateToProps = (state) => {
    return {}
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setAuthenticated
    }, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)