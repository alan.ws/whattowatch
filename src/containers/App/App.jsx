import React, { useState } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Redirect
} from "react-router-dom"
import Authenticate from '../Authentication'
import Search from '../Search'
import WatchList from '../WatchList'
import Welcome from '../Welcome'
import Authentication from '../../authentication/Authentication'
import NavBar from '../../components/NavBar'
// import styles from './App.module.css'


function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(false)

  const AuthHandler = new Authentication()
  AuthHandler.createRequestToken()
  AuthHandler.createAccessToken(setIsAuthenticated)

  function PrivateRoute({ component: Component, ...rest }) {
    return (
      <Route
        {...rest}
        render={props =>
          AuthHandler.isAuthenticated() ? (
            <Component {...props} />
          ) : (
              <Redirect
                to={{
                  pathname: "/authenticate",
                  state: {
                    from: props.location,
                    authHandler: AuthHandler,
                  }
                }}
              />
            )
        }
      />
    )
  }

  return (
    <>
      <Router>
        <NavBar authHandler={AuthHandler} />
        <Route exact path="/" component={Welcome} />
        <Route path="/authenticate" component={Authenticate} />
        <PrivateRoute exact path="/search" component={Search} />
        <PrivateRoute path="/watch-list" component={WatchList} />
      </Router>
    </>
  )
}

export default App