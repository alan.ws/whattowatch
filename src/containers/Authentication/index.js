import Authentication from './Authentication.jsx'
import { connect } from 'react-redux'
import { bindActionCreators } from "redux"

const mapStateToProps = (state) => {
    const { authHandler } = state.authenticationReducer
    return {
        authHandler
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({}, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Authentication)