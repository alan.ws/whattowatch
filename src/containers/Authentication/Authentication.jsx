import React, { useState } from 'react'
import { withRouter } from "react-router-dom"
// import styles from './Authentication.module.css'

function Authentication({ match, location, history }) {
    const [authenticationClicked, setAuthenticationClicked] = useState(false)
    const { requestToken } = location.state.authHandler

    return (
        <>
            <h1>You need to grant us access</h1>
            <p>We will redirect you to The Movie database for secure authentication</p>
            <a
                href={`https://www.themoviedb.org/auth/access?request_token=${requestToken}`}
                onClick={() => setAuthenticationClicked(true)}>Authenticate</a>
            {
                authenticationClicked
                    ? <p>Once authenticated, check out your TV shows</p>
                    : <></>
            }
        </>
    )
}

const AuthenticationWithRouter = withRouter(Authentication)

export default AuthenticationWithRouter