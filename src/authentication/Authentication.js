class Authentication {
    constructor() {
        this.requestToken = localStorage.getItem('request_token') || null
        this.apiKey =
            process.env.NODE_ENV === 'production'
                ? process.env.REACT_APP_API_KEY
                : process.env.REACT_APP_API_KEY_LOCALHOST
    }

    isAuthenticated() {
        const accessToken = localStorage.getItem('access_token')
        return accessToken ? true : false
    }

    async createRequestToken() {
        const requestToken = await localStorage.getItem('request_token')
        const accessToken = await localStorage.getItem('access_token')

        if (accessToken) {
            return
        }

        if (requestToken && this._isValidRequestToken()) {
            return
        }

        console.log('create request token')
        const timestamp = Date.now()

        const redirectUrl =
            process.env.NODE_ENV === 'production'
                ? process.env.REACT_APP_REDIRECT_URL
                : process.env.REACT_APP_REDIRECT_URL_LOCALHOST

        const requestTokenUrl = 'https://api.themoviedb.org/4/auth/request_token'
        const request = await fetch(requestTokenUrl, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${this.apiKey}`,
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({
                "redirect_to": `${redirectUrl}`
            })
        })
        // TODO: use for correct handling of errors and UX
        const responseStatus = await request.status
        const response = await request.json()

        if (response.success) {
            console.log('request token created')
            localStorage.setItem('request_token', await response.request_token)
            localStorage.setItem('request_token_timestamp', timestamp)
            localStorage.setItem('expired_request_token_timestamp', timestamp + 900)
        } else {
            console.log(response)
        }
    }

    async createAccessToken(isAuthenticatedCallback) {
        const accessToken = await localStorage.getItem('access_token')

        if (accessToken) {
            return
        }

        if (this._isValidRequestToken()) {
            console.log('create access token')
            const requestToken = await localStorage.getItem('request_token')
            const accessTokenUrl = 'https://api.themoviedb.org/4/auth/access_token'
            const request = await fetch(accessTokenUrl, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${this.apiKey}`,
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    "request_token": requestToken
                })
            })
            // TODO: use for correct handling of errors and UX
            const responseStatus = await request.status
            const response = await request.json()

            if (response.success) {
                console.log('store access token')
                localStorage.setItem('access_token', await response.access_token)
                isAuthenticatedCallback(true)
            } else {
                console.log(response)
            }
        } else {
            this.createRequestToken()
            this.createAccessToken()
        }
    }

    _currentTimestamp() {
        const timestamp = Date.now()
        return timestamp
    }

    async _retrieveRequestTokenExpiryTimeStamp() {
        return await localStorage.getItem('expired_request_token_timestamp')
    }

    _isValidRequestToken() {
        const accessToken = localStorage.getItem('access_token')
        const expiredRequestTokenTimestamp = localStorage.getItem('expired_request_token_timestamp')

        if (accessToken) {
            console.log('create request token still valid')
            return true
        }

        if (Number(expiredRequestTokenTimestamp) < this._currentTimestamp()) {
            console.log('create request token still valid')
            return true
        } else {
            console.log('create request token not valid')
            localStorage.removeItem('request_token')
            localStorage.removeItem('expired_request_token_timestamp')

            return false
        }
    }

    getRequestToken() {
        if (!this._isValidRequestToken()) {
            this.createRequestToken()
            return this.getRequestToken()
        }

        return this.requestToken
    }

    clear() {
        localStorage.clear()
    }
}

export default Authentication