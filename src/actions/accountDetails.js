export const setAccountId = (ACCOUNT_ID) => ({
    type: 'SET_ACCOUNT_ID',
    data: ACCOUNT_ID
})
