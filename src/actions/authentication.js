export const setAuthenticated = (isAuthenticated) => ({
    type: 'SET_AUTHENTICATED',
    data: isAuthenticated
})