const initialState = {
    accountId: null
}

export const accountDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_ACCOUNT_ID":
            return {
                ...state,
                accountId: action.data
            }

        default:
            return state
    }
}

export default accountDetailsReducer