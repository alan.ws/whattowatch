const initialState = {
    isAuthenticated: null
}

export const authenticationReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_AUTHENTICATED':
            return {
                ...state,
                isAuthenticated: action.data
            }

        default:
            return state
    }
}

export default authenticationReducer