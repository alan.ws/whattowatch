import NavBar from './NavBar.jsx'
import { connect } from 'react-redux'
import { bindActionCreators } from "redux"

const mapStateToProps = (state) => {
    const { isAuthenticated } = state.authenticationReducer
    return {
        isAuthenticated
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({}, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavBar)