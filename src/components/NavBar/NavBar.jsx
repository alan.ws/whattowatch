import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom"

function NavBar({ authHandler }) {
    const [isAuthenticated, setIsAuthenticated] = useState(authHandler.isAuthenticated())

    useEffect(() => {
        setIsAuthenticated(authHandler.isAuthenticated())
    }, [authHandler, isAuthenticated])

    return (
        <ul>
            <li>
                <Link to="/search">Search TV Shows</Link>
            </li>
            <li>
                <Link to="/watch-list">Your TV Watchlist</Link>
            </li>
            {
                isAuthenticated
                    ? (
                        <li>
                            Logged In
                            <button
                                // TODO: when we sign out, navigate to welcome screen or goodbye screen (UX)
                                onClick={() => {
                                    authHandler.clear()
                                    setIsAuthenticated(authHandler.isAuthenticated())
                                }}>Sign Out</button>
                        </li>
                    ) : (
                        <li>
                            {/* TODO: provide a login button */}
                            Not Logged In
                        </li>
                    )
            }
        </ul >
    )
}

export default NavBar
