import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import * as serviceWorker from './serviceWorker'

import './index.css'
import reducers from './reducers'

const store = createStore(reducers)

const App = React.lazy(() => import('./containers/App'))

ReactDOM.render(
    <Provider store={store}>
        <React.Suspense fallback={<div>Loading...</div>}>
            <App />
        </React.Suspense>
    </Provider>, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()